package com.mashibing.springboot.utils;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @author 钢人
 */
@Component
public class RedisUtils {

    private static final Logger logger = Logger.getLogger(RedisUtils.class);

    private static final String redisHashKey = "hash";

    @Autowired
    private RedisTemplate redisTemplate;
    //@Resource
    private JedisPool jedisPool;

    private static JedisPool staticJedisPool;

    private static RedisTemplate staticRedisTemplate;

    @PostConstruct
    private void init() {
        staticJedisPool = this.jedisPool;
        staticRedisTemplate = redisTemplate;
    }

    public static Jedis getResource() {
        if (staticJedisPool == null) {
            return null;
        }
        return staticJedisPool.getResource();
    }

    public static void returnResource(Jedis jedis) {
        if(jedis != null){
            staticJedisPool.returnResource(jedis);
        }
    }

    public static void set(String key, String value) {
        Jedis jedis=null;
        try{
            //jedis = getResource();
            jedis.set(key, value);
            HashOperations ops = staticRedisTemplate.opsForHash();
            ops.put(key, redisHashKey, value);
            logger.info("Redis set success - " + key + ", value:" + value);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Redis set error: "+ e.getMessage() +" - " + key + ", value:" + value);
        }finally{
            returnResource(jedis);
        }
    }

    public static String get(String key) {
        String result = null;
        Jedis jedis=null;
        try{
            jedis = getResource();
            // result = jedis.get(key);
            result = String.valueOf(staticRedisTemplate.opsForHash().get(key, redisHashKey));
            logger.info("Redis get success - " + key + ", value:" + result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Redis set error: "+ e.getMessage() +" - " + key + ", value:" + result);
        }finally{
            returnResource(jedis);
        }
        return result;
    }

}
