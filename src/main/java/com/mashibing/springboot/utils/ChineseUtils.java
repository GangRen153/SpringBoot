package com.mashibing.springboot.utils;

/**
 * @author 钢人
 */
public class ChineseUtils {

    public static String randomGenerate() {
        return randomGenerate(1, 10);
    }

    public static String randomGenerate(int min, int max) {
        int length = (int) (Math.random() * max);
        if (length < min) {
            length = min;
        }
        return generate(length);
    }

    public static String generate() {
        return generate(1);
    }

    public static String generate(int length) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            sb.append((char) (0x4e00 + (int) (Math.random() * (0x9fa5 - 0x4e00 + 1))));
        }
        return sb.toString();
    }

}
