package com.mashibing.springboot.other.reference;

import lombok.Data;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;

/**
 * 对象内存引用（强软弱虚）
 *
 * @author 钢人
 */
@Data
public class Reference {

    /**
     * 强引用（内存溢出也不回收）
     */
    private Object object = new Object();

    /**
     * 软引用
     */
    SoftReference<String> softReference = new SoftReference<>("软引用");

    /**
     * 弱引用
     */
    WeakReference<String> weakReference = new WeakReference<>("弱引用");

    static class MyPhantomReference<T> extends ReferenceQueue<Object> {
        public T t;
        public MyPhantomReference(T t) {
            this.t = t;
        }
    }

    MyPhantomReference<String> myPhantomReference = new MyPhantomReference<>("虚引用");

    /**
     * 虚引用
     */
    PhantomReference<MyPhantomReference<String>> phantomReference = new PhantomReference<>(myPhantomReference, myPhantomReference);

}
