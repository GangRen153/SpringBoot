package com.mashibing.springboot.other.threadLock;

import java.util.concurrent.Exchanger;

/**
 * 线程交换器
 * <p>
 * 两个线程直接交换数据
 *
 * @author 钢人
 */
public class T016_ThreadExchanger {
    public T016_ThreadExchanger() {
    }

    public static void main(String[] args) {
        Exchanger<String> exchanger = new Exchanger();
        Thread t1 = new Thread(() -> {
            try {
                String value = exchanger.exchange("t1");
                System.out.println("t1 -> " + value);
            } catch (InterruptedException var2) {
                var2.printStackTrace();
            }

        });
        t1.start();
        Thread t2 = new Thread(() -> {
            try {
                String value = exchanger.exchange("t2");
                System.out.println("t2 -> " + value);
            } catch (InterruptedException var2) {
                var2.printStackTrace();
            }

        });
        t2.start();
    }
}
