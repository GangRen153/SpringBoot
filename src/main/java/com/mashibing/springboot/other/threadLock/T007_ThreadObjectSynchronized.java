package com.mashibing.springboot.other.threadLock;

/**
 * 线程对象锁
 *
 * @author 钢人
 */
public class T007_ThreadObjectSynchronized {

    public static void main(String[] args) {
        final Object o = new Object();

        char[] aI = "1234567".toCharArray();
        char[] aC = "ABCDEFG".toCharArray();

        new Thread(() -> {
            // 只能有一个对象进入 synchronized，保证对象的访问安全
            synchronized (o) {
                for (char c : aI) {
                    System.out.print(c);
                    try {
                        // 通知 object 对象锁释放，让其他线程取获取锁
                        o.notify();
                        // 让出锁，进入锁等待
                        // 只有执行完这一步，object 对象锁才真正被释放，其他事线程才可获取这把锁
                        o.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                // 必须，否则无法停止程序
                o.notify();
            }
        }, "t1").start();

        new Thread(() -> {
            synchronized (o) {
                for (char c : aC) {
                    System.out.print(c);
                    try {
                        o.notify();
                        o.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                o.notify();
            }
        }, "t2").start();
    }

}
