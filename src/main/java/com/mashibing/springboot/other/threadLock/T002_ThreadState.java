package com.mashibing.springboot.other.threadLock;

import java.io.IOException;
import java.util.concurrent.locks.LockSupport;

/**
 * @author 钢人
 */
public class T002_ThreadState {
    public T002_ThreadState() {

    }

    public static void main(String[] args) throws Exception {
        Thread t = new Thread();
        t.start();
        // 就绪状态
        Thread.yield();
        // 睡眠
        Thread.sleep(1000L);
        // 无期限暂停当前线程
        LockSupport.park();
        // 取消暂停指定线程
        LockSupport.unpark(t);
        // 暂停当前线程
        LockSupport.park(null);
        // 暂停当前线程，不过有超时时间的限制（纳秒级别的）
        LockSupport.parkNanos(1000);
        // 暂停当前线程，不过有超时时间的限制（纳秒级别的）
        LockSupport.parkNanos(null, 1000);
        // 暂停当前线程，直到某个时间（毫秒级别的（经测试结论是睡眠不生效的））
        LockSupport.parkUntil(1000);
        // 暂停当前线程，直到某个时间（毫秒级别的）
        LockSupport.parkUntil(t, 1000);
    }
}
