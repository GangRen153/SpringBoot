package com.mashibing.springboot.other.threadLock;

import java.util.concurrent.Semaphore;

/**
 * 信号量线程，控制信号量，信号量等于0时，则其他线程再次获取的时候直接阻塞
 * 等其他线程取释放后，信号量恢复后才可获取到
 *
 * @author 钢人
 */
public class T015_ThreadSemaphore {
    volatile int count = 0;
    Semaphore semaphore = new Semaphore(1);

    public T015_ThreadSemaphore() {
    }

    public static void main(String[] args) {
        T015_ThreadSemaphore threadSemaphore = new T015_ThreadSemaphore();
        Thread thread = new Thread(() -> {
            try {
                // 获取信号量
                threadSemaphore.semaphore.acquire();
                Thread.sleep(500L);
                ++threadSemaphore.count;
                System.out.println("t1... -> " + threadSemaphore.count);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
            } finally {
                // 释放掉信号量
                threadSemaphore.semaphore.release();
            }

        });
        Thread thread1 = new Thread(() -> {
            try {
                threadSemaphore.semaphore.acquire();
                Thread.sleep(500L);
                ++threadSemaphore.count;
                System.out.println("t2... -> " + threadSemaphore.count);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
            } finally {
                threadSemaphore.semaphore.release();
            }

        });
        thread.start();
        thread1.start();
    }
}
