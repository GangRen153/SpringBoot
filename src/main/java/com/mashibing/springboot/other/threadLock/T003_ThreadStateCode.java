package com.mashibing.springboot.other.threadLock;

import java.io.IOException;

/**
 * @author 钢人
 */
public class T003_ThreadStateCode {
    public T003_ThreadStateCode() {
    }

    public static void main(String[] args) {
        Thread thread = new Thread();
        // 设置线程为中断状态，待线程执行结束后自动退出
        thread.interrupt();
        // 调用当前线程是否设置过中断状态
        thread.isInterrupted();
        // 恢复中断状态（目前没测出效果）
        Thread.interrupted();
        // 停止线程，直接中断
        thread.stop();
        // 暂停线程
        thread.suspend();
        // 恢复线程
        thread.resume();
    }
}