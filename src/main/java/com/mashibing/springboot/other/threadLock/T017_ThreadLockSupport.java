package com.mashibing.springboot.other.threadLock;

import java.util.concurrent.locks.LockSupport;

/**
 * @author 钢人
 */
public class T017_ThreadLockSupport {
    public T017_ThreadLockSupport() {
    }

    public static void main(String[] args) throws Exception {
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 10; ++i) {
                LockSupport.parkNanos(5000000000L);
                //Thread.sleep(200L);
                System.out.println(i);
            }
        });
        t1.start();
        // 阻塞

        while (true) {
            Thread.sleep(1000L);
            System.out.println(t1.getState());
            // 取消线程阻塞
            LockSupport.unpark(t1);
        }
    }
}
