package com.mashibing.springboot.other.threadLock;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

/**
 * @author 钢人
 */
public class T001_CreateThread {
    public T001_CreateThread() {
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        // 第一种方式
        (new Thread(() -> {
            System.out.println("1");
        })).start();

        // 第二种方式
        (new MyThread()).start();

        // 第三种方式
        (new Thread(new MyRunnable())).start();

        // 第四种方式
        FutureTask<String> task = new FutureTask(new MyCallable());

        // 第五种方式（带返回值的）
        Thread thread = new Thread(task);
        thread.start();
        String s = task.get();

        // 第六种方式（线程池带返回值的）
        ExecutorService callableService = Executors.newCachedThreadPool();
        Future<String> future = callableService.submit(new MyCallable());
        String value = future.get();

        // 第七种方式（不带返回值的）
        ExecutorService service = Executors.newCachedThreadPool();
        service.execute(() -> {
            System.out.println();
        });
        service.shutdown();
    }

    static class MyCallable implements Callable<String> {
        MyCallable() {
        }

        public String call() throws Exception {
            return "测试 Callable 线程返回参数";
        }
    }

    static class MyRunnable implements Runnable {
        MyRunnable() {
        }

        public void run() {
            System.out.println();
        }
    }

    static class MyThread extends Thread {
        MyThread() {
        }

        public void run() {
            super.run();
        }
    }
}

