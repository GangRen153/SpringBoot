package com.mashibing.springboot.other.proxy.aop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * @author 钢人
 */
@Component
public class A implements AImpl {

    @Lazy
    @Autowired
    A a;

    @Lazy
    @Autowired
    private B b;

    public A() {
    }

    @Override
    public void addUser() {
    }

    @Override
    public void editUser() {
    }
}