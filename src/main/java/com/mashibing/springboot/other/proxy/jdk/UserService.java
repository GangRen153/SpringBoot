package com.mashibing.springboot.other.proxy.jdk;

/**
 * @author 钢人
 */
public interface UserService {
    void add();

    void del();
}

