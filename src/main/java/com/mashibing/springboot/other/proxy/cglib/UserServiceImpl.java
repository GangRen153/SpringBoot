package com.mashibing.springboot.other.proxy.cglib;

/**
 * @author 钢人
 */
public class UserServiceImpl {
    public UserServiceImpl() {
    }

    public void add() {
        System.out.println("add...");
    }

    public void del() {
        System.out.println("del...");
    }
}
