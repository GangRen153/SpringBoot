package com.mashibing.springboot.other.proxy.aop;

/**
 * @author 钢人
 */
public interface AImpl {

    /**
     * 添加用户
     */
    void addUser();

    /**
     * 编辑用户
     */
    void editUser();
}

