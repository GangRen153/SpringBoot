package com.mashibing.springboot.base;

import lombok.Data;

/**
 * @author 钢人
 */
@Data
public class Result<T> {

    private Integer code;
    private Boolean success;
    private String message;
    private T data;

    public Result() {
        this(Boolean.TRUE);
    }

    public Result(Boolean success) {
        this(success ? CommonEnum.info : CommonEnum.error);
    }

    public Result(CommonEnum commonEnum) {
        this.code = commonEnum.getCode();
        this.success = commonEnum.getSuccess();
        this.message = commonEnum.getMessage();
    }

    public static <T> Result<T> newResult() {
        return new Result<>(CommonEnum.info);
    }

    public static <T> Result<T> newResult(Boolean success) {
        return new Result<>(success ? CommonEnum.info : CommonEnum.error);
    }

}
