package com.mashibing.springboot.base;

/**
 * 公共枚举
 *
 * @author 钢人
 */
public enum CommonEnum {

    /**
     * 状态信息
     */
    info(true, 200, "成功"), error(false, 500, "失败");

    private final Integer code;
    private final String message;
    private final Boolean success;

    CommonEnum(Boolean success, Integer code, String message) {
        this.code = code;
        this.message = message;
        this.success = success;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public Boolean getSuccess() {
        return success;
    }
}
