package com.mashibing.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * 继承 SpringBootServletInitializer 类是 tomcat war 方式启动，默认是 jar 包方式启动
 * 如果要打成 war 包用 tomcat 启动，则需要继承重写
 * @author 钢人
 */
@SpringBootApplication
@EnableMongoRepositories(basePackages = "com.*")
@ConfigurationPropertiesScan(basePackages = "com.*")
@EnableElasticsearchRepositories(basePackages = "com.*")
public class Application extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(Application.class);
	}

}
