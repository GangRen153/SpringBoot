package com.mashibing.springboot.dao.elasticsearch;

import com.alibaba.fastjson.JSON;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.ScriptType;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.*;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author 钢人
 */
@Configuration
public class BaseElasticsearchRepository<T> {

    AtomicLong atomicLong = new AtomicLong(System.currentTimeMillis());

    @Autowired
    private ElasticsearchRestTemplate template;

    public BaseElasticsearchRepository() {
    }

    public void createIndex(String... indexName) {
        boolean create = this.template.indexOps(IndexCoordinates.of(indexName)).create();
        System.out.println("createIndex " + Arrays.toString(indexName) + " -> " + create);
    }

    public void existsIndex(String... indexName) {
        boolean exists = this.template.indexOps(IndexCoordinates.of(indexName)).exists();
        System.out.println("existsIndex " + Arrays.toString(indexName) + " -> " + exists);
    }

    public void deleteIndex(String... indexName) {
        boolean delete = this.template.indexOps(IndexCoordinates.of(indexName)).delete();
        System.out.println("delete " + Arrays.toString(indexName) + " -> " + delete);
    }

    public void insert(T... users) {
        IndexCoordinates index = IndexCoordinates.of("user");
        this.test(users);
        T save = this.template.save(users[0]);
        this.test(users);
        T[] save1 = this.template.save(users, index);
        IndexQuery indexQuery = (new IndexQueryBuilder()).withId(String.valueOf(this.atomicLong.incrementAndGet())).withObject(users[0]).build();
        this.template.index(indexQuery, index);
        List<IndexQuery> listIndex = new ArrayList<>(users.length);
        this.test(users);
        int var9 = users.length;

        for (int var10 = 0; var10 < var9; ++var10) {
            T u = users[var10];
            IndexQuery batchInsert = new IndexQuery();
            batchInsert.setId(String.valueOf(System.currentTimeMillis()));
            batchInsert.setSource(JSON.toJSONString(u));
            listIndex.add(indexQuery);
        }

        this.template.bulkIndex(listIndex, index);
    }

    public void delete(T... users) {
        IndexCoordinates index = IndexCoordinates.of("user");
        BoolQueryBuilder price = QueryBuilders.boolQuery().must(QueryBuilders.termQuery("brand", "huawei"));
        NativeSearchQuery query = new NativeSearchQuery(price);
        this.template.delete(query, users[0].getClass());
    }

    public void update(T... users) {
        IndexCoordinates index = IndexCoordinates.of("user");
        NativeSearchQueryBuilder search = new NativeSearchQueryBuilder();
        BoolQueryBuilder bool = QueryBuilders.boolQuery().must(QueryBuilders.termsQuery("brand", "oppo", "huawei"));
        NativeSearchQuery query = search.withSourceFilter(new FetchSourceFilter(new String[]{"name"}, (String[]) null)).withQuery(bool).build();
        Map<String, Object> params = new HashMap<>(1);
        params.put("price", 2051);
        UpdateQuery scriptUpdate = UpdateQuery.builder(query).withScriptType(ScriptType.INLINE).withParams(params).withScript("if(ctx._source.containsKey('price')){ctx._source.price = params.price}").build();
        this.template.updateByQuery(scriptUpdate, index);
    }

    public List<T> list(Integer currentPage, Integer limit, Class<T> clazz) {
        NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
        NativeSearchQuery query = builder.withQuery(QueryBuilders.matchAllQuery()).build();
        if (false) {
            query = builder.withQuery(QueryBuilders.boolQuery().must(QueryBuilders.termQuery("brand", "oppo"))).build();
        }
        if (currentPage != null && limit != null) {
            query.setPageable(PageRequest.of(currentPage, limit));
        }
        if (false) {
            query.addSort(Sort.by(new Order(Direction.DESC, "price")));
        }

        List<SearchHit<T>> searchHits = template.search(query, clazz).getSearchHits();
        List<T> result = new ArrayList<>();
        for (SearchHit<T> searchHit : searchHits) {
            result.add(searchHit.getContent());
        }
        return result;
    }

    public void test(T... users) {
        for (T user : users) {
            try {
                user.getClass().getField("id").set(user, this.atomicLong.incrementAndGet());
            } catch (IllegalAccessException | NoSuchFieldException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
