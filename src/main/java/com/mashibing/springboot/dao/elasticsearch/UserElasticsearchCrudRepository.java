package com.mashibing.springboot.dao.elasticsearch;

import com.mashibing.springboot.entity.elasticsearch.ElasticsearchUser;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;

/**
 * @author 钢人
 */
@Component
public interface UserElasticsearchCrudRepository extends ElasticsearchRepository<ElasticsearchUser, String> {

}
