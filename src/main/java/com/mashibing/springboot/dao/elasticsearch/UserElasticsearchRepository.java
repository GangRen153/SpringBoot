package com.mashibing.springboot.dao.elasticsearch;

import com.mashibing.springboot.entity.elasticsearch.ElasticsearchUser;
import org.springframework.context.annotation.Configuration;

/**
 * @author 钢人
 */
@Configuration
public class UserElasticsearchRepository extends BaseElasticsearchRepository<ElasticsearchUser> {
}
