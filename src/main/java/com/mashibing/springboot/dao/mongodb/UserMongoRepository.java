package com.mashibing.springboot.dao.mongodb;

import com.mashibing.springboot.entity.mongodb.MongoUser;
import com.mashibing.springboot.entity.mysql.UserEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author 钢人
 */
public interface UserMongoRepository extends MongoRepository<UserEntity, String> {
}
