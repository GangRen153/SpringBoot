package com.mashibing.springboot.dao.mongodb;

import com.mashibing.springboot.entity.mysql.UserEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author 钢人
 */
public interface BaseMongoRepository extends MongoRepository<UserEntity, String> {
}