package com.mashibing.springboot.dao.mysql.base;

import com.mashibing.springboot.entity.mysql.UserEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 钢人
 */
public interface BaseEntityDAO<K, T, P> {
    long countByParam(P example);

    int deleteByParam(P example);

    int deleteByPrimaryKey(K id);

    int insert(T record);

    int insertSelective(T record);

    List<T> selectByParam(P example);

    UserEntity selectByPrimaryKey(K id);

    int updateByParamSelective(@Param("record") T record, @Param("example") P example);

    int updateByParam(@Param("record") T record, @Param("example") P example);

    int updateByPrimaryKeySelective(T record);

    int updateByPrimaryKey(T record);

    int batchInsert(List<T> list);
}