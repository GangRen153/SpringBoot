package com.mashibing.springboot.dao.mysql;

import com.mashibing.springboot.entity.QuestionBank;
import com.mashibing.springboot.entity.QuestionBankParam;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BaseQuestionBankDAO {
    long countByParam(QuestionBankParam example);

    int deleteByParam(QuestionBankParam example);

    int deleteByPrimaryKey(Long id);

    int insert(QuestionBank record);

    int insertSelective(QuestionBank record);

    List<QuestionBank> selectByParam(QuestionBankParam example);

    QuestionBank selectByPrimaryKey(Long id);

    int updateByParamSelective(@Param("record") QuestionBank record, @Param("example") QuestionBankParam example);

    int updateByParam(@Param("record") QuestionBank record, @Param("example") QuestionBankParam example);

    int updateByPrimaryKeySelective(QuestionBank record);

    int updateByPrimaryKey(QuestionBank record);

    int batchInsert(List<QuestionBank> list);
}