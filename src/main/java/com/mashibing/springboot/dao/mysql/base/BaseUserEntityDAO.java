package com.mashibing.springboot.dao.mysql.base;

import com.mashibing.springboot.dao.mysql.param.UserEntityParam;
import com.mashibing.springboot.entity.mysql.UserEntity;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

public interface BaseUserEntityDAO {
    long countByParam(UserEntityParam example);

    int deleteByParam(UserEntityParam example);

    int deleteByPrimaryKey(Integer id);

    int insert(UserEntity record);

    int insertSelective(UserEntity record);

    List<UserEntity> selectByParam(UserEntityParam example);

    UserEntity selectByPrimaryKey(Integer id);

    int updateByParamSelective(@Param("record") UserEntity record, @Param("example") UserEntityParam example);

    int updateByParam(@Param("record") UserEntity record, @Param("example") UserEntityParam example);

    int updateByPrimaryKeySelective(UserEntity record);

    int updateByPrimaryKey(UserEntity record);

    int batchInsert(List<UserEntity> list);
}