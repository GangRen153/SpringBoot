package com.mashibing.springboot.dao.mysql;

import com.mashibing.springboot.dao.mysql.base.BaseEntityDAO;
import com.mashibing.springboot.dao.mysql.param.UserEntityParam;
import com.mashibing.springboot.entity.mysql.UserEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 钢人
 */
@Mapper
public interface UserEntityDAO extends BaseEntityDAO<Integer, UserEntity, UserEntityParam> {
}
