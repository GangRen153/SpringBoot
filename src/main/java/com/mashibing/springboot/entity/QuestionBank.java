package com.mashibing.springboot.entity;

public class QuestionBank {
    /**
     * 
     */
    private Long id;

    /**
     * 
     */
    private String title;

    /**
     * 
     */
    private String result;

    /**
     * 
     */
    private String options;

    /**
     * 题型
     */
    private String type;

    /**
     * 课程
     */
    private String course;

    /**
     * 
     * @return id 
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     *@param id 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 
     * @return title 
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     *@param title 
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * 
     * @return result 
     */
    public String getResult() {
        return result;
    }

    /**
     * 
     *@param result 
     */
    public void setResult(String result) {
        this.result = result == null ? null : result.trim();
    }

    /**
     * 
     * @return options 
     */
    public String getOptions() {
        return options;
    }

    /**
     * 
     *@param options 
     */
    public void setOptions(String options) {
        this.options = options == null ? null : options.trim();
    }

    /**
     * 题型
     * @return type 题型
     */
    public String getType() {
        return type;
    }

    /**
     * 题型
     *@param type 题型
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * 课程
     * @return course 课程
     */
    public String getCourse() {
        return course;
    }

    /**
     * 课程
     *@param course 课程
     */
    public void setCourse(String course) {
        this.course = course == null ? null : course.trim();
    }
}