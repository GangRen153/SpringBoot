package com.mashibing.springboot.entity.mysql;

public class UserEntity {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 姓名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 主键
     * @return id 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 主键
     *@param id 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 姓名
     * @return username 姓名
     */
    public String getUsername() {
        return username;
    }

    /**
     * 姓名
     *@param username 姓名
     */
    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    /**
     * 密码
     * @return password 密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 密码
     *@param password 密码
     */
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    /**
     * 年龄
     * @return age 年龄
     */
    public Integer getAge() {
        return age;
    }

    /**
     * 年龄
     *@param age 年龄
     */
    public void setAge(Integer age) {
        this.age = age;
    }
}