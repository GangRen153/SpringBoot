package com.mashibing.springboot.entity.mongodb;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;
import java.util.List;

/**
 * @author 钢人
 */
@Data
@Document(indexName = "user")
public class MongoUser {

    private String id;
    private String brand;
    private String model;
    private Integer ram;
    private Double price;
    private Date manufactureDate;
    private String content;
    private List<String> tags;
    private String introduce;

}
