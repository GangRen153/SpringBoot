package com.mashibing.springboot.controller;

import com.mashibing.springboot.base.Result;
import com.mashibing.springboot.dao.elasticsearch.UserElasticsearchRepository;
import com.mashibing.springboot.entity.elasticsearch.ElasticsearchUser;
import com.mashibing.springboot.entity.mongodb.MongoUser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.mashibing.springboot.entity.mysql.UserEntity;
import com.mashibing.springboot.service.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 钢人
 */
@RestController
@RequestMapping("/home")
public class HomeController {

    @Autowired
    private HomeService service;
    @Resource
    private UserElasticsearchRepository repository;

    @GetMapping("/test")
    public Result<?> test() {
        return Result.newResult(Boolean.TRUE);
    }

    @GetMapping("/test2")
    public String test2() {
        return "测试接口";
    }

    @GetMapping({"/createIndex"})
    public Result<?> createIndex() {
        this.repository.createIndex("user");
        return Result.newResult();
    }

    @GetMapping({"/insert"})
    public Result<?> insert(ElasticsearchUser user) {
        this.repository.insert(this.createUser(user));
        return new Result<>(Boolean.TRUE);
    }

    @GetMapping({"/update"})
    public Result<?> update(ElasticsearchUser user) {
        this.repository.update(user);
        return new Result<>(Boolean.TRUE);
    }

    @GetMapping({"/delete"})
    public Result<?> delete(ElasticsearchUser user) {
        this.repository.delete(this.createUser(user));
        return new Result<>(Boolean.TRUE);
    }

    @GetMapping({"list"})
    public Result<?> list() {
        this.repository.list(1, 3, ElasticsearchUser.class);
        return new Result<>(Boolean.TRUE);
    }

    public ElasticsearchUser[] createUser(ElasticsearchUser user) {
        ElasticsearchUser[] users = new ElasticsearchUser[3];
        for(int i = 0; i < 3; ++i) {
            ElasticsearchUser elasticsearchUser = new ElasticsearchUser();
            elasticsearchUser.setId(user.getId());
            elasticsearchUser.setBrand(user.getBrand());
            elasticsearchUser.setContent(user.getContent());
            elasticsearchUser.setModel(user.getModel());
            elasticsearchUser.setPrice(user.getPrice());
            elasticsearchUser.setRam(user.getRam());
            elasticsearchUser.setIntroduce(user.getIntroduce());
            elasticsearchUser.setManufactureDate(new Date());
            users[i] = elasticsearchUser;
        }
        return users;
    }

    @GetMapping("/mysql")
    public List<MongoUser> mysql(){
        service.mysqlTest();
        return Collections.emptyList();
    }

    @GetMapping("/mongodb")
    public List<UserEntity> mongodb(UserEntity user){
        List<UserEntity> list = new ArrayList<>();
        list.add(service.mongoTest(user));
        return list;
    }

    @GetMapping("/redis")
    public List<String> redis(String key, String value){
        List<String> list = new ArrayList<>();
        list.add(service.redisTest(key, value));
        return list;
    }

    @GetMapping("/elasticsearch")
    public List<ElasticsearchUser> elasticsearch(ElasticsearchUser user){
        return service.elasticsearchTest(user);
    }

}
