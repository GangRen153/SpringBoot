package com.mashibing.springboot.service;

import com.mashibing.springboot.dao.elasticsearch.UserElasticsearchCrudRepository;
import com.mashibing.springboot.dao.elasticsearch.UserElasticsearchRepository;
import com.mashibing.springboot.dao.mongodb.UserEntityRepository;
import com.mashibing.springboot.dao.mysql.UserEntityDAO;
import com.mashibing.springboot.entity.elasticsearch.ElasticsearchUser;
import com.mashibing.springboot.entity.mysql.UserEntity;
import com.mashibing.springboot.utils.ChineseUtils;
import com.mashibing.springboot.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 钢人
 */
@Service
public class HomeService {

    @Autowired
    private UserEntityDAO dao;
    @Autowired
    private UserEntityRepository userMongoRepository;
    @Autowired
    private UserElasticsearchRepository elasticsearchRepository;
    @Autowired
    private UserElasticsearchCrudRepository elasticsearchCrudRepository;

    public UserEntity mongoTest(UserEntity user) {
        user.setId(1);
        user.setUsername("mongodb");
        return userMongoRepository.save(user);
    }

    public String redisTest(String key, String value) {
        RedisUtils.set(key, value + " - version");
        return RedisUtils.get(key);
    }

    public List<ElasticsearchUser> elasticsearchTest(ElasticsearchUser user) {
        user.setId("1662961906465");
        Page<ElasticsearchUser> users = elasticsearchCrudRepository.searchSimilar(user, new String[]{"id", "brand"}, new PageRequest(0, 100, Sort.unsorted()) {
        });
        return elasticsearchRepository.list(0, 100, ElasticsearchUser.class);
    }

    public void mysqlTest() {
        System.out.println(ChineseUtils.randomGenerate(2, 4));
    }

}
